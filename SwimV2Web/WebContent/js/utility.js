$(document).ready(function() {
	var switchRegisterButton = $('#login #switchRegisterButton');
	var switchLoginButton = $('#registerForm #switchLoginButton');
	var loginButton = $('#login #loginButton');
	var slider = $('#formSlider');
	var sliderContent = $('.formcontainer');
	var pageMessage = $('#content #pageMessage');
	var pageMessageClose = $('#content .pageMessageClose');
	var searchInputField = $('#searchBar input[type="text"]');
	var sendFriendshipRequestButton = $('.sendFriendshipRequestButton');
	var sendHelpRequestButton = $('.sendHelpRequestButton');
	var searchButton = $('#searchButton');
	var editDataButton = $('#editUserDataButton');
	var editDataContent = $('#editUserDataContent');
	var replyTextarea = $('.viewRequestContent textarea');
	var feedbackTextarea = $('.viewResponseContent textarea');	
	var listButton = $('.personalPageList h1');
	var addAbilityButton = $('#addAbilityButton');
	var deleteAbilityButton = $('.deleteAbilityButton');
	var addThisAbilityButton = $('.addThisAbilityButton');
	
	addThisAbilityButton.click(function(){
		console.log("Adding ability...");
		abilityId = $('.abilityCombo').val();
		abilityName = $('.abilityId'+abilityId).html();
		userId = $('#userId').attr("value");
		$.ajax({
			url: './AddUserAbilityServlet',
			type: 'post',
			dataType: 'text',
			data: {userId: userId, abilityId: abilityId },
			success: function(data) {
				var result = eval(data); //result sar� true se data == "true", false se data =="false"
				if(result==true){					
					$('.userSkills ul').append('<li class="userSkill'+abilityId+'"><div class="userSkill"><span class="abilityName">'+abilityName+'</span><form style="display:inline;" action="#"><input id="abilityName" type="hidden" value="'+abilityName+'"><input id="abilityId" type="hidden" name="abilityId" value="'+abilityId+'"><input id="userId" type="hidden" name="userId" value="'+userId+'"><img class="deleteAbilityButtonAdded'+abilityId+' deleteAbilityButton" src="img/bt_delete.png"></form></div></li>');
					$('.abilityId'+abilityId).remove();
					deleteAbilityButtonAdded = $('.deleteAbilityButtonAdded'+abilityId);
					deleteAbilityButtonAdded.click(function(){
						console.log("Deleting ability...");
						parent = $(this).parent();
						var abilityId = parent.children('#abilityId').attr("value");
						var userId = parent.children('#userId').attr("value");
						var abilityName = parent.children('#abilityName').attr("value");
						$.ajax({
							url: './DeleteAbilityServlet',
							type: 'post',
							dataType: 'text',
							data: {userId: userId, abilityId: abilityId },
							success: function(data) {
								var result = eval(data); //result sar� true se data == "true", false se data =="false"
								if(result==true){			
									console.log($('.userSkill'+abilityId));
									$('.userSkill'+abilityId).remove();
									$('.abilityCombo').append('<option class="abilityId'+abilityId+'" value="'+abilityId+'">'+abilityName+'</option>')
								}else if(result==false){
									console.log("DELETE SKILL ERROR");
								}
							}
						});
					});
				}else if(result==false){
					console.log("ADD SKILL ERROR");
				}
			}
		});
	});
	
	deleteAbilityButton.click(function(){
		console.log("Deleting ability...");
		parent = $(this).parent();		
		var abilityId = parent.children('#abilityId').attr("value");
		var userId = parent.children('#userId').attr("value");
		var abilityName = parent.children('#abilityName').attr("value");
		$.ajax({
			url: './DeleteAbilityServlet',
			type: 'post',
			dataType: 'text',
			data: {userId: userId, abilityId: abilityId },
			success: function(data) {
				var result = eval(data); //result sar� true se data == "true", false se data =="false"
				if(result==true){
					$('.userSkill'+abilityId).remove();
					$('.abilityCombo').append('<option class="abilityId'+abilityId+'" value="'+abilityId+'">'+abilityName+'</option>')
				}else if(result==false){
					console.log("DELETE SKILL ERROR");
				}
			}
		});
	});
	
	
	addAbilityButton.click(function(){
		var userId = $('#userId').attr("value");
		console.log(userId);
		$(this).parent().append('<div class="addAbilityMessageContent"><form method="post" action="./SendRequestServlet"><img class="addAbilityMessageClose" src="img/close.png"><textarea class="addAbilityMessage" name="message">Write a message to request a new ability specifying why you need it...</textarea><input type="hidden" name="requestType" value="ability"><input type="hidden" name="sender" value="'+userId+'"><input type="submit" value="send"></form></div>');
		var addAbilityMessageCloseButton = $('.addAbilityMessageClose');		
		addAbilityMessageCloseButton.click(function(){
			$('.addAbilityMessageContent').fadeOut(1000, function(){
				$('.addAbilityMessageContent').remove();				
			});
		});
		var addAbilityMessage = $('.addAbilityMessage');
		addAbilityMessage.focus(function(){
			if($(this).html()=="Write a message to request a new ability specifying why you need it..."){
				$(this).html("");
				$(this).css("color","#000");
			}
		});
	});
	
	replyTextarea.focus(function(){
		if($(this).html()=='Write a reply...'){
			$(this).html("");
			$(this).css("color","#000");
		}
	});
	
	feedbackTextarea.focus(function(){
		if($(this).html()=='Write a feedback...'){
			$(this).html("");
			$(this).css("color","#000");
		}
	});
	
	listButton.click(function(){		
		if($(this).html().substring(0,1) == '+'){			
			$(this).parent().children('.listContent').slideDown(1500);
			html = 
			$(this).html('-'+$(this).html().substring(1));
		}else if($(this).html().substring(0,1) == '-'){
			$(this).parent().children('.listContent').slideUp(1500);
			$(this).html('+'+$(this).html().substring(1));
		}
	});
	
	editDataButton.click(function(){
		if($(this).html() == "+ Edit your data"){
			editDataContent.slideDown('slow');
			$(this).html("- Edit your data");
		}else if(($(this).html() == "- Edit your data")){
			editDataContent.slideUp('slow');
			$(this).html("+ Edit your data");
		}
	});
	
	searchButton.click(function(){		
		$(this).parent("#searchBar").submit();
	});
	
	sendHelpRequestButton.click(function(){
		pageMessage.fadeOut(1000);
		$(this).parent().append('<div class="requestMessage"><h1><a class="requestMessageClose" href="#"><img src="img/close.png"></a>Request Message</h1><textarea name="message"></textarea><div class="buttonRequestContent"><input type="submit" value="Send Message Request"></div></div>');
		var requestMessageCloseButton = $('#content .requestMessageClose');
		requestMessageCloseButton.click(function(){
			$('.requestMessage').fadeOut(1000, function(){
				$('.requestMessage').remove();
			});
		});
	});
	
	sendFriendshipRequestButton.click(function(){		
		pageMessage.fadeOut(1000);
		$(this).parent().append('<div class="requestMessage"><h1><a class="requestMessageClose" href="#"><img src="img/close.png"></a>Request Message</h1><textarea name="message"></textarea><div class="buttonRequestContent"><input type="submit" value="Send Friendship Request"></div></div>');
		var requestMessageCloseButton = $('#content .requestMessageClose');
		requestMessageCloseButton.click(function(){
			$('.requestMessage').fadeOut(1000, function(){
				$('.requestMessage').remove();
			});
		});
	});
	
	searchInputField.focus(function(){
		if(searchInputField.attr("value")=="insert ability or username..." || searchInputField.attr("value")=="insert ability..."){
			searchInputField.attr("value","");
		}
	});	
	switchRegisterButton.click(function(){
		slider.animate({left: '-450px'},1000);
		sliderContent.animate({height: '325px'},1000);
		pageMessage.fadeOut(1000,function(){
			pageMessage.remove();
		});		
	});
	switchLoginButton.click(function(){
		slider.animate({left: '20px'},1000);
		sliderContent.animate({height: '220px'},1000);
		pageMessage.fadeOut(1000,function(){
			pageMessage.remove();
		});
	});
	loginButton.click(function(){	
		pageMessage.fadeOut(1000,function(){
			pageMessage.remove();
		});		
	});
	pageMessageClose.click(function(){	
		pageMessage.fadeOut(1000,function(){
			pageMessage.remove();
		});		
	});
	
	
});