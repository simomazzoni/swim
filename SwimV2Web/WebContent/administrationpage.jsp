<%@ include file="frags/headers.jsp" %>
	<title>SWIMv2 | Administration Page</title>
</head>
<%@ include file="frags/top.jsp" %>
<%@ include file="frags/initialContent.jsp" %>
<%
if (request.getSession().getAttribute("loggedadmin") == null) {
	response.sendRedirect("./");
} else {
	%>
	<% /*AFTER THIS LINE BEGINS THE CONTENT OF THE PAGE*/ %>
	<div id="administrationPage">
	<div class="topRow clearfix">
		<div class="userPicture">
			<img src="users_pictures/admin.png" alt="${loggedadmin.username}_picture"/>
		</div>
		<div class="userData">
			<div class="dataBox">
				<div class="userDataRow"><span class="fieldName">E-mail: </span><span class="fieldData">${loggedadmin.email}</span></div>
				<div class="userDataRow"><span class="fieldName">Username: </span><span class="fieldData">${loggedadmin.username}</span></div>
			</div>
		</div>	
	</div>
	<div id="abilitySet">
	<span class="abilitiesFieldName">List of all abilities</span>
	<form method="post" action="./AddNewAbilityServlet">
		<input type="text" name="abilityname">
		<input type="submit" value="Declare New Ability">
	</form>
	<ul>
	<%List<Ability> abilities = abilman.listAllAbilities();
	for(Ability a: abilities){%>
		<li class="userSkill<%= a.getId()%>">
			<div class="userSkill">
			<span class="abilityName"><%= a.getName()%></span>
			</div>
		</li>
		<%}%>
	</ul>	
	</div>
	<%
	List<AbilityRequest> abilityrequests = reqman.viewAbilityRequests();	
	if(!abilityrequests.isEmpty()){%>
	<div id="abilityRequestsList" class="personalPageList">
		<h1>+ Ability Requests (<%= abilityrequests.size()%>)</h1>
		<%
		int i=0;
		%><div class="listContent"><%
		for (AbilityRequest r: abilityrequests) {
			if(i%2==0){%>
				<div class="requestWhite clearfix">
					<form method="post" action="viewrequest.jsp">
						<img src="<%= r.getSender().getPicture()%>"/>
						<p>from <%= r.getSender().getUsername()%></p>
						<p class="date">(<%= r.getDate().toString().substring(0,10)%>)</p>
						<input type="hidden" name="id_request" value="<%= r.getId()%>">
						<input type="hidden" name="requestType" value="ability">
						<p style="text-align:right;"><input type="submit" value="View"></p>
					</form>
				</div>
			<%}else{%>
				<div class="requestGray clearfix">
					<form method="post" action="viewrequest.jsp">
						<img src="<%= r.getSender().getPicture()%>"/> 
						<p>from <%= r.getSender().getUsername()%></p>
						<p class="date">(<%= r.getDate().toString().substring(0,10)%>)</p>
						<input type="hidden" name="id_request" value="<%= r.getId()%>">
						<input type="hidden" name="requestType" value="ability">
						<p style="text-align:right;"><input type="submit" value="View"></p>
					</form>
				</div>
			<%}
			i++;
		}%>
		</div>
	</div>
	<%}	%>
<%} %>
</div>
<%@ include file="frags/footer.jsp" %>