<%@ include file="frags/headers.jsp" %>
	<title>SWIMv2 | Small World hypothesIs Machine v2</title>
</head>
<%@ include file="frags/top.jsp" %>
<%@ include file="frags/initialContent.jsp" %>
<%
if (request.getSession().getAttribute("loggeduser") != null) {
	response.sendRedirect("personalpage.jsp");
} else if (request.getSession().getAttribute("loggedadmin") != null) {
	response.sendRedirect("administrationpage.jsp");
}
%>

<% /*AFTER THIS LINE BEGINS THE CONTENT OF THE PAGE*/ %>
<div class="formcontainer">
	<div id="formSlider">
	<div class="formSliderObj">			
		<form id="login" method="post" action="./LoginServlet">
			<div class="formrow"><label for="username">USERNAME</label><label for="password">PASSWORD</label></div>
			<div class="formrow"><input type="text" name="username" style="margin-right:10px;"><input type="password" name="password"></div>
			<div class="formrow"><span style="font-size:10px;display:block;text-align:left;margin-left:10px;float:left;"><input type="checkbox" name="admin" value="admin">Admin Login</span><span style="font-size:10px;display:block;text-align:left;margin-left:230px;"><input type="checkbox" name="keepmelogged" value="keepmelogged">Keep me logged in</span></div>					
			<div class="formrow" style="margin-bottom:30px;"><input id="loginButton" type="submit" value="Login"></div>
			<div class="formrow">OR</div>
			<div class="formrow"><button id="switchRegisterButton" type="button">Register</button></div>
		</form>
	</div>
	<div class="formSliderObj">
		<form id="register" method="post" enctype="multipart/form-data" action="./RegistrationServlet">
			<div class="formrow"><div class="formMessage"></div></div>
			<div class="formrow"><label for="firstname">*FIRST NAME</label><label for="lastname">*LAST NAME</label></div>
			<div class="formrow"><input type="text" name="firstname" style="margin-right:10px;"><input type="text" name="lastname"></div>
			<div class="formrow"><label for="username">*USERNAME</label><label for="password">*PASSWORD</label></div>
			<div class="formrow"><input type="text" name="username" style="margin-right:10px;"><input type="password" name="password"></div>
			<div class="formrow"><label for="mail">*MAIL</label><label for="picture">PICTURE</label></div>
			<div class="formrow"><input type="text" name="mail" style="margin-right:10px;"><input type="file" name="picture"></div>
			<div class="formrow" style="margin-top:15px;text-align:left;margin-bottom:30px;"><input id="registerFormButton" type="button" value="Register"></div>
			<div class="formrow">OR</div>
			<div class="formrow"><button id="switchLoginButton" type="button">Login</button></div>
		</form>
	</div>
	</div>
</div>
<%@ include file="frags/footer.jsp" %>