<%@ include file="frags/headers.jsp" %>
	<title>SWIMv2 | Response</title>
</head>
<%@ include file="frags/top.jsp" %>
<%@ include file="frags/initialContent.jsp" %>
<% /*AFTER THIS LINE BEGINS THE CONTENT OF THE PAGE*/ %>
<div id="viewResponsePage">
<% 
if (request.getParameter("responseType").equals("help")) {
	HelpResponse rep = repman.viewHelpResponse(Long.parseLong(request.getParameter("id_response")));
	%>
	<h1><span class="viewResponseUsername"><%= rep.getRequest().getReceiver().getUsername()%></span> has <% if(rep.getResponse()==true){ %>accepted<% }else{ %>declined<% } %> your help request.</h1>
	<div class="viewResponseContent">
		<p class="username"><%= rep.getRequest().getReceiver().getUsername()%> says...</p>
		<div class="viewResponseMessage"><%=rep.getMessage()%></div>
		<p class="username">Your original request message...</p>
		<div class="viewResponseMessage"><%=rep.getRequest().getMessage()%></div>
		<form method="post" action="./HandleNotificationServlet">
			<input type="hidden" name="responseType" value="help">
			<input type="hidden" name="id_request" value=<%= rep.getRequest().getId() %>>
			<div style="text-align:center;margin-bottom:10px;">
				<input class="ok" type="submit" name="result" value="Ok">
				<input class="cancel" type="submit" name="result" value="Cancel">
			</div>
		</form>
	</div>		
	<%
} else if (request.getParameter("responseType").equals("friendship")) {
	FriendshipResponse rep = repman.viewFriendshipResponse(Long.parseLong(request.getParameter("id_response")));
	%>
	<h1><span class="viewResponseUsername"><%= rep.getRequest().getReceiver().getUsername()%></span> has <% if(rep.getResponse()==true){ %>accepted<% }else{ %>declined<% } %> your friendship request.</h1>	
	<div class="viewResponseContent">
		<p class="username"><%= rep.getRequest().getReceiver().getUsername()%> says...</p>
		<div class="viewResponseMessage"><%=rep.getMessage()%></div>
		<p class="username">Your original request message...</p>
		<div class="viewResponseMessage"><%=rep.getRequest().getMessage()%></div>
		<form method="post" action="./HandleNotificationServlet">
			<input type="hidden" name="responseType" value="friendship">
			<input type="hidden" name="id_request" value=<%= rep.getRequest().getId() %>>
			<% if (rep.getResponse()==true){ %>
			<input type="hidden" name="response" value="true">
			 <% } %>
			<div style="text-align:center;margin-bottom:10px;">
				<input class="ok" type="submit" name="result" value="Ok">
				<input class="cancel" type="submit" name="result" value="Cancel">
			</div>
		</form>
	</div>	
	<%	
} else if (request.getParameter("responseType").equals("ability")) {
	AbilityResponse rep = repman.viewAbilityResponse(Long.parseLong(request.getParameter("id_response")));
	%>
	<h1>An Administrator has <% if(rep.getResponse()==true){ %>accepted<% }else{ %>declined<% } %> your help request.</h1>
	<div class="viewResponseContent">
		<p class="username">The Administrator says...</p>	
		<div class="viewResponseMessage"><%=rep.getMessage()%></div>
		<p class="username">Your original request message...</p>
		<div class="viewResponseMessage"><%=rep.getRequest().getMessage()%></div>
		<form method="post" action="./HandleNotificationServlet">
			<input type="hidden" name="responseType" value="ability">
			<input type="hidden" name="id_request" value=<%= rep.getRequest().getId() %>>
			<div style="text-align:center;margin-bottom:10px;">
				<input class="ok" type="submit" name="result" value="Ok">
				<input class="cancel" type="submit" name="result" value="Cancel">
			</div>
		</form>
	</div>	
	<%
} else if (request.getParameter("responseType").equals("feedback")) {
	HelpResponse rep = repman.viewHelpResponse(Long.parseLong(request.getParameter("id_response")));
	%>
	<h1><span class="viewResponseUsername"><%= rep.getRequest().getReceiver().getUsername()%></span> has <% if(rep.getResponse()==true){ %>accepted<% }else{ %>declined<% } %> your help request.</h1>
	<div class="viewResponseContent">
		<p class="username"><%= rep.getRequest().getReceiver().getUsername()%> says...</p>
		<div class="viewResponseMessage"><%=rep.getMessage()%></div>
		<p class="username">Your original request message...</p>
		<div class="viewResponseMessage"><%=rep.getRequest().getMessage()%></div>
		<form method="post" action="./SendFeedbackServlet">
			<p class="username" style="float: left;margin-right: 10px;line-height: 40px;">Rate this response:</p>
			<select name="rate" class="rate">
				<option value="1">1</option>
				<option value="2">2</option>
				<option value="3">3</option>
				<option value="4">4</option>
				<option value="5">5</option>
			</select>
			<textarea name="message">Write a feedback...</textarea>			
			<input type="hidden" name="id_response" value=<%= rep.getId() %>>
			<div style="text-align:center;margin-bottom:10px;">
				<input class="ok" type="submit" name="result" value="Send Feedback">
				<input class="cancel" type="submit" name="result" value="Cancel">
			</div>
		</form>
	</div>		
	<%
}
%>
</div>
<%@ include file="frags/footer.jsp" %>