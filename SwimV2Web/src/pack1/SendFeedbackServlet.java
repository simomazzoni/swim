package pack1;

import java.io.IOException;

import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import swim.entitybeans.FriendshipRequest;
import swim.sessionbeans.FriendshipManagerRemote;
import swim.sessionbeans.RequestRemote;
import swim.sessionbeans.ResponseRemote;

/**
 * Servlet implementation class SendFeedbackServlet
 */
public class SendFeedbackServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SendFeedbackServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			if (request.getParameter("result").equals("Cancel")) {
				response.sendRedirect("personalpage.jsp");
			} else {
				InitialContext jndiContext = new InitialContext();
				ResponseRemote repman = (ResponseRemote) jndiContext.lookup("Response/remote");
				long id_response = Long.parseLong(request.getParameter("id_response"));
				String message = request.getParameter("message");
				int rate = Integer.parseInt(request.getParameter("rate"));
				repman.sendFeedback(id_response, rate, message);
				request.getSession().setAttribute("pageMessage", "Your feedback was succesfully sent!");
				request.getSession().setAttribute("pageMessageType", "success");
				response.sendRedirect("personalpage.jsp");
			}		
		}catch(Exception e) {
			e.printStackTrace();
		}
	
	}

}
