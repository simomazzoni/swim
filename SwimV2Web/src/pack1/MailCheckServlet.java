package pack1;

import java.io.IOException;
import java.util.List;
import java.util.Set;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import swim.entitybeans.RegisteredUser;
import swim.sessionbeans.SearchRemote;

/**
 * Servlet implementation class MailChechServlet
 */
public class MailCheckServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MailCheckServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			System.out.println("MailCheck");
			InitialContext jndiContext = new InitialContext();
			SearchRemote src = (SearchRemote) jndiContext.lookup("Search/remote");			
			String mail = request.getParameter("mail");
			Set<RegisteredUser> users = src.searchByMail(mail);
			String mailOK = null;
			if(users.isEmpty()){
				mailOK = "true";
			}else{
				mailOK = "false";
			}
			response.setContentType("text/plain");  
			response.setCharacterEncoding("UTF-8"); 
			response.getWriter().write(mailOK);
			//request.getRequestDispatcher("index.html").forward(request, response);
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
