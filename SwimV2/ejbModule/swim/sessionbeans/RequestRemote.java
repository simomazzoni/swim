package swim.sessionbeans;

import java.util.List;

import javax.ejb.Remote;

import swim.entitybeans.AbilityRequest;
import swim.entitybeans.FriendshipRequest;
import swim.entitybeans.HelpRequest;
import swim.entitybeans.RegisteredUser;

@Remote
public interface RequestRemote {
	
	public boolean sendHelpRequest(long id_sender, long id_receiver, String message);
	
	public boolean sendFriendshipRequest(long id_sender, long id_receiver, String message, boolean suggested);
	
	public boolean sendAbilityRequest(long id_sender, String message);
	
	public List<HelpRequest> viewHelpRequests(long id_receiver);
	
	public List<FriendshipRequest> viewFriendshipRequests(long id_receiver);
	
	public List<AbilityRequest> viewAbilityRequests();
	
	public HelpRequest viewHelpRequest(long id_request);
	
	public FriendshipRequest viewFriendshipRequest(long id_request);
	
	public AbilityRequest viewAbilityRequest(long id_request);
	
	public void setNotificated(long id_request, String requestType);

}
