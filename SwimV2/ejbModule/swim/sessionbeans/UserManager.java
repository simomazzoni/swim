package swim.sessionbeans;

import java.util.*;

import javax.ejb.Stateless;
import javax.persistence.*;

import org.jboss.ejb3.annotation.RemoteBinding;

import swim.entitybeans.RegisteredUser;
import swim.entitybeans.Ability;
/**
 * Session Bean implementation class UserManager
 */
@Stateless
@RemoteBinding(jndiBinding="UserManager/remote")
public class UserManager implements UserManagerRemote, UserManagerLocal {

	@PersistenceContext(unitName = "swim")
	private EntityManager em;
	
	
    /**
     * Default constructor. 
     */
    public UserManager() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public boolean addNewUser(String username, String password, String email, String firstname, String lastname, String picture) {
		RegisteredUser newUser = new RegisteredUser();
		newUser.setUsername(username);
		newUser.setPassword(password);
		newUser.setEmail(email);
		newUser.setFirstname(firstname);
		newUser.setLastname(lastname);
		newUser.setPicture(picture);
		try {
			em.persist(newUser);
		}
		catch(Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	@SuppressWarnings("unchecked")
	public Set<Ability> getSkills (long userId) {
		HashSet<Ability> skills = new HashSet<Ability>();
		Query q = em.createQuery("SELECT a FROM RegisteredUser u JOIN u.skills a WHERE  u.id = :userId");		
		q.setParameter("userId", userId);
		skills.addAll(q.getResultList());
		return skills;
	}
	
	public boolean deleteSkill (long userId, long abilityId) {
		boolean result = false;
		RegisteredUser u = em.find(RegisteredUser.class, userId);
		result = u.getSkills().remove(em.find(Ability.class, abilityId));
		em.flush();
		return result;
	}
	
	public boolean addSkill (long userId, long abilityId) {
		boolean result = false;
		RegisteredUser u = em.find(RegisteredUser.class, userId);
		result = u.getSkills().add(em.find(Ability.class, abilityId));
		em.flush();
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<Ability> getDeclarableSkills (long userId) {
		List<Ability> skills = new ArrayList<Ability>();
		Query q = em.createQuery("SELECT a FROM Ability a WHERE a NOT IN (SELECT s FROM RegisteredUser u JOIN u.skills s WHERE  u.id = :userId)");		
		q.setParameter("userId", userId);
		skills.addAll(q.getResultList());
		return skills;
	}
	
	public void editPersonalInfo(long id,String username, String password, String email, String firstname, String lastname, String picture) {
		RegisteredUser editUser = em.find(RegisteredUser.class, id);
		if (username!=null)
			editUser.setUsername(username);
		if (password!=null)
			editUser.setPassword(password);
		if (email!=null)
			editUser.setEmail(email);
		if (firstname!=null)
			editUser.setFirstname(firstname);
		if (lastname!=null)
			editUser.setLastname(lastname);
		if (picture!=null)
			editUser.setPicture(picture);
		em.flush();
		em.refresh(editUser);
	}

}
