package swim.sessionbeans;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.jboss.ejb3.annotation.RemoteBinding;

import swim.entitybeans.*;


/**
 * Session Bean implementation class Response
 */
@Stateless
@RemoteBinding(jndiBinding="Response/remote")
public class Response implements ResponseRemote, ResponseLocal {

	@PersistenceContext(unitName="swim")
	private EntityManager manager;
	
    /**
     * Default constructor. 
     */
    public Response() {
        // TODO Auto-generated constructor stub
    }
    
    public boolean sendHelpResponse(long id_request,boolean response, String message){
			
		HelpResponse newHelpRes = new HelpResponse();
		HelpRequest req = manager.find(HelpRequest.class, id_request);
		newHelpRes.setRequest(req);
		newHelpRes.setMessage(message);
		newHelpRes.setResponse(response);
		try {
			manager.persist(newHelpRes);
			req.setState(true);
			manager.persist(req);
		}
		catch(Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
    
    public boolean sendFriendshipResponse(long id_request,boolean response, String message){
		
		FriendshipResponse newFrindRes = new FriendshipResponse();
		FriendshipRequest req = manager.find(FriendshipRequest.class, id_request);
		newFrindRes.setRequest(req);
		newFrindRes.setMessage(message);
		newFrindRes.setResponse(response);
		try {
			manager.persist(newFrindRes);
			req.setState(true);
			manager.persist(req);
		}
		catch(Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

    public boolean sendAbilityResponse(long id_request, boolean response, String message, long id_admin){
		
    	AbilityResponse newAbilityRes = new AbilityResponse();
    	AbilityRequest req = manager.find(AbilityRequest.class, id_request);
		newAbilityRes.setRequest(req);
		newAbilityRes.setAdmin(manager.find(Admin.class, id_admin));
		newAbilityRes.setMessage(message);
		newAbilityRes.setResponse(response);
		try {
			manager.persist(newAbilityRes);
			req.setState(true);
			manager.persist(req);
		}
		catch(Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
    
    
    @SuppressWarnings("unchecked")
	public List<HelpResponse> viewHelpResponses(long id_receiver){
		List<HelpResponse> HR = new ArrayList<HelpResponse>();
		try {
			RegisteredUser receiver = manager.find(RegisteredUser.class, id_receiver);
			Query q = manager.createQuery("SELECT h FROM HelpResponse h JOIN h.request hreq WHERE hreq.sender =:receiver AND hreq.state = true AND hreq.notification = false");
			q.setParameter("receiver", receiver);
			HR.addAll(q.getResultList());
			return HR;			
		}
		catch (Exception ex) {
			ex.printStackTrace();
			return HR;
		}
	}
		

	@SuppressWarnings("unchecked")
	public List<FriendshipResponse> viewFriendshipResponses(long id_receiver){
		List<FriendshipResponse> FR = new ArrayList<FriendshipResponse>();
		try {
			RegisteredUser receiver = manager.find(RegisteredUser.class, id_receiver);
			Query q = manager.createQuery("SELECT f FROM FriendshipResponse f JOIN f.request freq WHERE freq.sender =:receiver AND freq.state = true AND freq.notificated = false");
			q.setParameter("receiver", receiver);
			FR.addAll(q.getResultList());
			return FR;			
		}
		catch (Exception ex) {
			ex.printStackTrace();
			return FR;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<AbilityResponse> viewAbilityResponses(long id_receiver){
		List<AbilityResponse> AR = new ArrayList<AbilityResponse>();
		try {
			RegisteredUser receiver = manager.find(RegisteredUser.class, id_receiver);
			Query q = manager.createQuery("SELECT a FROM AbilityResponse a JOIN a.request areq WHERE areq.sender =:receiver AND areq.state = true AND areq.notificated = false");
			q.setParameter("receiver", receiver);
			AR.addAll(q.getResultList());
			return AR;			
		}
		catch (Exception ex) {
			ex.printStackTrace();
			return AR;
		}
	}
 
 
	public HelpResponse viewHelpResponse(long id_response){
		try {
			return manager.find(HelpResponse.class, id_response);
		}
		catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}
    
  	public FriendshipResponse viewFriendshipResponse(long id_response){
  		try {
  			return manager.find(FriendshipResponse.class, id_response);
  		}
  		catch (Exception ex) {
  			ex.printStackTrace();
  			return null;
  		}
  	}
    
  	public AbilityResponse viewAbilityResponse(long id_response){
  		try {
  			return manager.find(AbilityResponse.class, id_response);
  		}
  		catch (Exception ex) {
  			ex.printStackTrace();
  			return null;
  		}
  	}
 
    public boolean sendFeedback(long id_response, int rate, String message){
		Feedback feedback = new Feedback();
    	feedback.setResponse(manager.find(HelpResponse.class, id_response));
    	feedback.setMessage(message);
    	feedback.setRate(rate);
		try {
			manager.persist(feedback);
		}	
		catch(Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
    
    @SuppressWarnings("unchecked")
   	public List<HelpResponse> viewResponsesForFeedback(long id_receiver){
   		List<HelpResponse> HR = new ArrayList<HelpResponse>();
   		try {
   			RegisteredUser receiver = manager.find(RegisteredUser.class, id_receiver);
   			Query q = manager.createQuery("SELECT h FROM HelpResponse h JOIN h.request hreq WHERE h.response = true AND hreq.sender =:receiver AND hreq.state = true AND hreq.notification = true AND h NOT IN (SELECT hf FROM Feedback f JOIN f.response hf)");
   			q.setParameter("receiver", receiver);
   			HR.addAll(q.getResultList());
   			return HR;			
   		}
   		catch (Exception ex) {
   			ex.printStackTrace();
   			return HR;
   		}
   	}
    
}
