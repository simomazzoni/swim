package swim.sessionbeans;

import java.util.List;
import java.util.Set;

import javax.ejb.Remote;

import swim.entitybeans.Ability;

@Remote
public interface UserManagerRemote {
	
	public boolean addNewUser(String username, String password, String email, String firstname, String lastname, String picture);
		
	public Set<Ability> getSkills (long userId);
	
	public boolean deleteSkill (long userId, long skillId);
	
	public void editPersonalInfo(long id,String username, String password, String email, String firstname, String lastname, String picture);
	
	public boolean addSkill (long userId, long abilityId);
	
	public List<Ability> getDeclarableSkills (long userId);
}
