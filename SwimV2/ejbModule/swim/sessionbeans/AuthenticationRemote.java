package swim.sessionbeans;

import javax.ejb.Remote;

import swim.entitybeans.RegisteredUser;
import swim.entitybeans.Admin;

@Remote
public interface AuthenticationRemote {

	 public RegisteredUser AuthenticateUser(String email, String password);

	 public Admin AuthenticateAdmin(String email, String password);

}
