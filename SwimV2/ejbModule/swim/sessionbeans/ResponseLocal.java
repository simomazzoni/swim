package swim.sessionbeans;

import java.util.List;

import javax.ejb.Local;

import swim.entitybeans.AbilityResponse;
import swim.entitybeans.FriendshipResponse;
import swim.entitybeans.HelpRequest;
import swim.entitybeans.HelpResponse;

@Local
public interface ResponseLocal {

	 public boolean sendHelpResponse(long id_request,boolean response, String message);
	 
	 public boolean sendFriendshipResponse(long id_request,boolean response, String message);
	 
	 public boolean sendAbilityResponse(long id_request, boolean response, String message, long id_admin);

	 public HelpResponse viewHelpResponse(long id_response);
	 
	 public FriendshipResponse viewFriendshipResponse(long id_response);
	 
	 public AbilityResponse viewAbilityResponse(long id_response);

	 public List<HelpResponse> viewHelpResponses(long id_receiver);
	 
	 public List<FriendshipResponse> viewFriendshipResponses(long id_receiver);
	 
	 public List<AbilityResponse> viewAbilityResponses(long id_receiver);
	 
	 public boolean sendFeedback(long id_response, int rate, String message);
	 
	 public List<HelpResponse> viewResponsesForFeedback(long id_receiver);

}
