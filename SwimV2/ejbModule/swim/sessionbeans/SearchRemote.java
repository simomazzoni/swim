package swim.sessionbeans;

import java.util.Set;
import java.util.List;

import javax.ejb.Remote;

import swim.entitybeans.Admin;
import swim.entitybeans.RegisteredUser;

@Remote
public interface SearchRemote {
	public Set<RegisteredUser> searchUser(String name);

	public Set<RegisteredUser> searchForAbility(String ability);
	
	public Set<RegisteredUser> searchByUsername(String username);
	
	public Set<RegisteredUser> searchByMail(String mail);
	
	public RegisteredUser searchById(long id);
	
	public Admin searchAdminById(long id);
}
