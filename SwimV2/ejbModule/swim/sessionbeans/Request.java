package swim.sessionbeans;

import javax.ejb.Stateless;
import javax.persistence.*;

import org.jboss.ejb3.annotation.RemoteBinding;

import swim.entitybeans.*;

import java.sql.Date;

import java.util.ArrayList;
import java.util.List;

/**
 * Session Bean implementation class Request
 */

@Stateless
@RemoteBinding(jndiBinding="Request/remote")
public class Request implements RequestRemote, RequestLocal {
	
	@PersistenceContext(unitName="swim")
	private EntityManager manager;

    /**
     * Default constructor. 
     */
    public Request() {
        // TODO Auto-generated constructor stub
    }
public boolean sendHelpRequest(long id_sender, long id_receiver, String message){	
		HelpRequest newHelpReq = new HelpRequest();
		RegisteredUser sender = manager.find(RegisteredUser.class,id_sender);
		RegisteredUser receiver = manager.find(RegisteredUser.class,id_receiver);
		newHelpReq.setSender(sender);
		newHelpReq.setReceiver(receiver);
		newHelpReq.setMessage(message);
		newHelpReq.setNotification(false);
		newHelpReq.setDate(new Date(System.currentTimeMillis() ));
		newHelpReq.setState(false);
		try {
			manager.persist(newHelpReq);
		}
		catch(Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean sendFriendshipRequest(long id_sender, long id_receiver, String message, boolean suggested){
		FriendshipRequest newFriendReq = new FriendshipRequest();
		RegisteredUser sender = manager.find(RegisteredUser.class, id_sender);
		RegisteredUser receiver = manager.find(RegisteredUser.class, id_receiver);
		newFriendReq.setSender(sender);
		newFriendReq.setReceiver(receiver);
		newFriendReq.setMessage(message);
		newFriendReq.setNotificated(false);
		newFriendReq.setDate(new Date(System.currentTimeMillis() ));
		newFriendReq.setState(false);
		newFriendReq.setSuggested(suggested);
		try {
			manager.persist(newFriendReq);
		}
		catch(Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean sendAbilityRequest(long id_sender, String message){
		AbilityRequest newAbilityReq = new AbilityRequest();
		RegisteredUser sender = manager.find(RegisteredUser.class, id_sender);
		newAbilityReq.setSender(sender);
		newAbilityReq.setMessage(message);
		newAbilityReq.setNotificated(false);
		newAbilityReq.setDate(new Date(System.currentTimeMillis() ));
		newAbilityReq.setState(false);	
		try {
			manager.persist(newAbilityReq);
		}
		catch(Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	@SuppressWarnings("unchecked")
	public List<HelpRequest> viewHelpRequests(long id_receiver){
		List<HelpRequest> HR = new ArrayList<HelpRequest>();
		try {
			RegisteredUser receiver = manager.find(RegisteredUser.class, id_receiver);
			Query q = manager.createQuery("FROM HelpRequest h WHERE h.receiver =:receiver AND h.state = false");
			q.setParameter("receiver", receiver);
			HR.addAll(q.getResultList());
			return HR;			
		}
		catch (Exception ex) {
			ex.printStackTrace();
			return HR;
		}
	}
		

	@SuppressWarnings("unchecked")
	public List<FriendshipRequest> viewFriendshipRequests(long id_receiver){
		List<FriendshipRequest> FR = new ArrayList<FriendshipRequest>();
		try {
			RegisteredUser receiver = manager.find(RegisteredUser.class, id_receiver);
			Query q = manager.createQuery("FROM FriendshipRequest f WHERE f.receiver =:receiver AND f.state = false");
			q.setParameter("receiver", receiver);
			FR.addAll(q.getResultList());
			return FR;			
		}
		catch (Exception ex) {
			ex.printStackTrace();
			return FR;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<AbilityRequest> viewAbilityRequests(){
		List<AbilityRequest> AR = new ArrayList<AbilityRequest>();
		try {
			Query q = manager.createQuery("FROM AbilityRequest a WHERE a.state = false");
			AR = q.getResultList();
			return AR;
		}
		catch (Exception ex) {
			ex.printStackTrace();
			return AR;
		}
	}
	
	public HelpRequest viewHelpRequest(long id_request) {
		try {
			return manager.find(HelpRequest.class, id_request);
		}
		catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}
	
	public FriendshipRequest viewFriendshipRequest(long id_request){
		try {
			return manager.find(FriendshipRequest.class, id_request);
		}
		catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}		
	}
	
	public AbilityRequest viewAbilityRequest(long id_request){
		try {
			return manager.find(AbilityRequest.class, id_request);
		}
		catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public void setNotificated(long id_request, String requestType) {
		try {
			if (requestType.equals("help")) {
				HelpRequest req = manager.find(HelpRequest.class, id_request);
				req.setNotification(true);
				manager.flush();
			} else if (requestType.equals("friendship")) {
				FriendshipRequest req = manager.find(FriendshipRequest.class, id_request);
				req.setNotificated(true);
				manager.flush();
			} else if (requestType.equals("ability")) {
				AbilityRequest req = manager.find(AbilityRequest.class, id_request);
				req.setNotificated(true);
				manager.flush();
			}
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
	}

    
}
