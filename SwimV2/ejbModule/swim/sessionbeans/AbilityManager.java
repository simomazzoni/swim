package swim.sessionbeans;

import javax.ejb.Stateless;
import javax.persistence.*;

import org.jboss.ejb3.annotation.RemoteBinding;

import java.util.ArrayList;
import java.util.List;
import swim.entitybeans.*;
/**
 * Session Bean implementation class AbilityManager
 */
@Stateless
@RemoteBinding(jndiBinding="AbilityManager/remote")
public class AbilityManager implements AbilityManagerRemote, AbilityManagerLocal {

	@PersistenceContext(unitName = "swim")
	private EntityManager em;
	
	
    /**
     * Default constructor. 
     */
    public AbilityManager() {
        // TODO Auto-generated constructor stub
    }

    
    public boolean addNewAbility(String name) {
		Ability newAbility = new Ability();
		newAbility.setName(name);
		try {
			em.persist(newAbility);
		}
		catch(Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
    
    @SuppressWarnings("unchecked")
    public List<Ability> listAllAbilities() {
    	List<Ability> abilities = new ArrayList<Ability>();
    	try {
    		Query q = em.createQuery("SELECT a FROM Ability a");
    		abilities.addAll(q.getResultList());
    		return abilities;
    	} catch (Exception ex) {
   			ex.printStackTrace();
   			return abilities;
   		}
   	}
}
