package swim.entitybeans;


import javax.persistence.*;


@Entity
@Table(name = "friendshipresponse")
public class FriendshipResponse implements java.io.Serializable {
	
	@Id
	@GeneratedValue
	@Column(name = "ID")	
	private long id;
	
	@OneToOne(targetEntity=swim.entitybeans.FriendshipRequest.class)
	@JoinColumn(name = "id_friendship_request")
	private FriendshipRequest request;
	
	@Column(name = "response")	
	private boolean response;
	
	@Column(name = "message", columnDefinition = "LONGTEXT")	
	private String message;
	
	public long getId() {
		return id;
	}
	
	public FriendshipRequest getRequest() {
		return request;
	}

	public void setRequest(FriendshipRequest request) {
		this.request = request;
	}
	
	public boolean getResponse() {
		return response;
	}

	public void setResponse(boolean response) {
		this.response = response;
	}
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	

}
