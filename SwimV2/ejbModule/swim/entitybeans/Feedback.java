package swim.entitybeans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "feedback")
public class Feedback implements java.io.Serializable {
	
	@Id
	@GeneratedValue
	@Column(name = "ID")	
	private long id;
	
	//@Column(name = "id_help_response")	
	//private int id_help_response;
	
	@ManyToOne(targetEntity=swim.entitybeans.HelpResponse.class)
	@JoinColumn(name = "id_help_response")
	private HelpResponse response;
	
	@Column(name = "rate")	
	private int rate;

	@Column(name = "message", columnDefinition = "LONGTEXT")	
	private String message;
	
	public long getId() {
		return id;
	}
	
	public HelpResponse getResponse() {
		return response;
	}

	public void setResponse(HelpResponse response) {
		this.response = response;
	}
	
	public int getRate() {
		return rate;
	}

	public void setRate(int rate) {
		this.rate = rate;
	}
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
